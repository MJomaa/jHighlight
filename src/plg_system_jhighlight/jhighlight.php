<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemJHighlight extends JPlugin
{

    function plgSystemAdd2Home(& $subject, $config)
    {
        parent::__construct($subject, $config);
        $this->loadLanguage();
    }

    public function onBeforeRender()
    {
        $app = JFactory::getApplication();
        if ($app->isAdmin()) {
            return true;
        }

        $document = JFactory::getDocument();

        $document->addScript("/plugins/system/jhighlight/js/highlight.min.js");

        $skins = $this->params->get('skins', 'default.min.css');
        $document->addStyleSheet("/plugins/system/jhighlight/styles/$skins");

        $document->addScriptDeclaration("\nhljs.initHighlightingOnLoad();\n");
    }

}

# jHighlight
jHighlight is a Joomla plugin and syntax highlighter to help Joomla webmaster highlight code syntax.

It uses https://highlightjs.org/

Demo (63 skins): https://highlightjs.org/static/demo/

+1 Skin (Visual Studio Dark)


``Installation``
----------------------

Download pkg_jhighlight_VERSION.zip

Go into your Backend-> Extensions-> Extensions Manager-> Upload Package File and **upload** it.

``Enable``
----------------------

Don't forget to **enable** it.

Go into your Backend-> Plugins-> System  and enable "System - jHighlight"

``How to use``
----------------------

    <pre><code class="csharp">
        YOUR CODE HERE
    </code></pre>

(Sometimes it's better to **toogle the Editor off** to insert a "raw" snippet)

There are currently **125 languages** supported.

**Css-class-names** for those can be found here: http://highlightjs.readthedocs.org/en/latest/css-classes-reference.html

``Example:``
----------------------

(It's much sharper than this jpg)
![Example](example.jpg)


``Skins``
----------------------

There are 64 skins to choose from. See here for reference: https://highlightjs.org/static/demo/

Go to Backend-> Plugins and open "System - jHighlight".

Choose a skin and press "Save & close"



``Official Release Article``
----------------------

http://mjomaa.com/projects/80-jhighlight
